#insert data to users
INSERT INTO users (email,password,datetime_created) VALUES ("johnsmith@gmail.com","passwordA",2021-1-1 01:00:00);
INSERT INTO users (email,password,datetime_created) VALUES ("juandelacruz@gmail.com","passwordB",2021-1-1 02:00:00);
INSERT INTO users (email,password,datetime_created) VALUES ("janesmith@gmail.com","passwordC",2021-1-1 03:00:00);
INSERT INTO users (email,password,datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD",2021-1-1 04:00:00);
INSERT INTO users (email,password,datetime_created) VALUES ("johndoe@gmail.com","passwordE",2021-1-1);

#insert data to posts
INSERT INTO posts (author_id,title,content,datetime_posted) VALUES (1,"First Code","Hello World!", 2021-1-2 01:00:00);
INSERT INTO posts (author_id,title,content,datetime_posted) VALUES (1,"Second Code","Hello Earth!", 2021-1-2 01:00:00);
INSERT INTO posts (author_id,title,content,datetime_posted) VALUES (2,"Third Code","Welcome to Mars!", 2021-1-2 01:00:00);
INSERT INTO posts (author_id,title,content,datetime_posted) VALUES (4,"Fourth Code","Bye bye Solar System", 2021-1-2 01:00:00);

#queries
SELECT * FROM posts WHERE author_id = 1;
SELECT email, datetime_created FROM users;
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE author_id = 1 and content ="Hello Earth!";
DELETE FROM users WHERE email="johndoe@gmail.com";